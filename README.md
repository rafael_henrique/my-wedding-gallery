## My Wedding Gallery

This project was made to participate of a test. It was made using Python, Pyramid framework, MongoDB, Vue and Docker. The choice for Pyramid was simply to know the framework and learn how it works, Flask or Django could be a more confortable choice though. Vue was chosen because of its ease of use and configure. Without Docker the deployment would be much more hard to do.

I preferred not to put the requirements here. I'm afraid this might break the rules.

Before start running, clone the repository
```
git clone https://rafael_henrique@bitbucket.org/rafael_henrique/my-wedding-gallery.git
```

open `docker-compose.yml` and insert your Amazon S3 Access Keys in there

#### Running in development mode:
To run the backend app execute:
```sh
pip install -r requirements.txt
pserve --reload development.ini
```

To run the frontend app, go to `my-wedding-galley-app` folder and execute:
```sh
npm install
npm run serve
```

#### Running in production mode:
Configure the server URL by modifying the .env.production file situated in `my-wedding-galley-app` folder

then just run:
docker-compose up -d