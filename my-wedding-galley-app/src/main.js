import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import axios from "axios";
import Notifications from 'vue-notification'
import { VLazyImagePlugin } from "v-lazy-image";
import Lightbox from 'vue-my-photos'

// Default Axios Config
axios.defaults.baseURL = process.env.VUE_APP_API_URL
axios.defaults.headers.common['Content-Type'] = 'application/json'
const TOKEN = store.getters.user.token
if (TOKEN) {
  axios.defaults.headers.common.Authorization = `Bearer ${TOKEN}`
}

// Interceptador de unauthorized response
axios.interceptors.response.use(response => {
  return response
}, function (error) {
  if (error.response.status === 401) {
    store.dispatch('logout')
    router.push({ name: 'Login' })
  }
  return Promise.reject(error)
})

Vue.config.productionTip = false
Vue.use(Notifications)
Vue.use(VLazyImagePlugin)
Vue.component('lightbox', Lightbox);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
