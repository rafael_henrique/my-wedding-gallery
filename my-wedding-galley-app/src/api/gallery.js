import axios from 'axios'

export default {
    fetchGalleryImages() {
        return axios.get('/gallery')
    },
    approveImage(id) {
        return axios.put('/gallery/'+id+'/approve')
    },
    addLikeToImage(id) {
        return axios.put('/gallery/'+id+'/like')
    }
}