import axios from 'axios'
import users from "@/api/users"

const STORAGE_KEY = 'anchor-loans'

const state = {
  user: JSON.parse(window.localStorage.getItem(STORAGE_KEY) || '{}')
}

const mutations = {
  SET_USER (state, user) {
    state.user = user

    window.localStorage.setItem(STORAGE_KEY, JSON.stringify(user))

    axios.defaults.headers.common.Authorization = `Bearer ${state.user.token}`
  },
  UNSET_USER (state) {
    state.user = {}

    window.localStorage.setItem(STORAGE_KEY, '{}')

    delete axios.defaults.headers.common.Authorization
  },
  UPDATE_USER_INFO (state, user) {
    Object.assign(state.user, user)
  }
}

const actions = {
  login ({ commit }, { username, password }) {
    return users.login({ username, password })
      .then(response => {
        commit('SET_USER', response.data)
      })
  },
  logout ({ commit }) {
    commit('UNSET_USER')
  }
}

const getters = {
  user: state => state.user,
  isAuthenticated: state => Object.keys(state.user).length > 0
}

export default {
  state,
  getters,
  mutations,
  actions
}
