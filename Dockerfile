FROM python:3.7.3-alpine3.9

COPY . /var/www/

WORKDIR /var/www

RUN apk --update add python py-pip openssl ca-certificates py-openssl wget
RUN apk --update add --virtual build-dependencies libffi-dev openssl-dev python-dev py-pip build-base \
  && pip install --upgrade pip \
  && pip install -r requirements.txt \
  && apk del build-dependencies

RUN python setup.py install

CMD ["pserve", "production.ini"]