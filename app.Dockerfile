FROM nginx:alpine

# RUN apt-get update && apt-get install -y npm
RUN apk update && apk add npm 

COPY ./my-wedding-galley-app/ /var/www/
COPY docker/app/nginx.conf /etc/nginx/

WORKDIR /var/www

RUN npm install
RUN npm rebuild node-sass

RUN npm run build