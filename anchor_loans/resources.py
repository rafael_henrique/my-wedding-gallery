from pyramid.security import Everyone, Authenticated, Allow

from anchor_loans.services.gallery_images_service import GalleryImagesService

class Root(object):

    __acl__ = [
        (Allow, Everyone, "view"),
        (Allow, Authenticated, "authenticated"),
    ]

    def __init__(self, request):
        self.request = request
