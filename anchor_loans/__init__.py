from pyramid.config import Configurator
from pyramid.authorization import ACLAuthorizationPolicy

from anchor_loans.resources import Root

from urllib.parse import urlparse

from pymongo import MongoClient


def main(global_config, **settings):
    config = Configurator(settings=settings)
    config.set_authorization_policy(ACLAuthorizationPolicy())
    config.set_root_factory(Root)

    config.include("cornice")
    config.include('pyramid_jwt')
    config.set_jwt_authentication_policy('secret', auth_type='Bearer')

    db_url = urlparse(settings['mongo_uri'])
    config.registry.db = MongoClient(
        host=db_url.hostname,
        port=db_url.port,
    )

    def add_db(request):
        db = config.registry.db[db_url.path[1:]]
        if db_url.username and db_url.password:
            db.authenticate(db_url.username, db_url.password)
        return db

    config.add_request_method(add_db, 'db', reify=True)

    config.scan("anchor_loans.views")
    return config.make_wsgi_app()
