import os
import uuid
from datetime import datetime
from passlib.hash import pbkdf2_sha256 as sha256

class UserService():

    def __init__(self, db):
        self.db = db['users']

    def save(self, user):
        user['password'] = sha256.hash(user['password'])
        user["createdAt"] = datetime.utcnow()
        user["modifiedAt"] = datetime.utcnow()
        self.db.insert(user)
        return user

    def find_by_username(self, username):
        return self.db.find_one({"username": username})

    def find_by_username_and_password(self, username, password):
        user = self.find_by_username(username)
        if user:
            if sha256.verify(password, user['password']):
                return user

    def find_by_id(self, id):
        return self.db.find_one({"_id": id})
