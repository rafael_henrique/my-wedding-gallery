from anchor_loans.amazon_s3 import client
import os
import uuid
from datetime import datetime

S3_URL = "https://s3-sa-east-1.amazonaws.com/anchorloans-gallery/"

class GalleryImagesService():

    bucket = "anchorloans-gallery"

    def __init__(self, db):
        self.db = db['gallery_images']

    def save(self, file, user_id):
        filename, extension = os.path.splitext(file.filename)
        filename = f"{str(uuid.uuid4())}{extension}"

        client.upload_fileobj(file.file, self.bucket, filename)

        image = {
            "filename": filename,
            "path": S3_URL + filename,
            "approved": False,
            "createdBy": user_id,
            "createdAt": datetime.utcnow(),
            "modifiedAt": datetime.utcnow(),
            "likes": []
        }

        self.db.insert(image)
        return image

    def list_images(self, criteria={}):
        return self.db.find(criteria)

    def list_approved_images(self):
        return self.list_images({"approved": True})

    def find_by_id(self, id):
        return self.db.find_one({"_id": id})

    def delete(self, id):
        image = self.find_by_id(id)
        client.delete_object(Bucket=self.bucket, Key=image['filename'])
        return self.db.remove({"_id": id})
    
    def approve(self, id, user_id):
        image = {
            "approved": True,
            "approvedBy": user_id,
            "modifiedAt": datetime.utcnow()
        }

        self.db.update_one({"_id": id}, {"$set": image})
        return self.find_by_id(id)

    def add_like(self, id, user_id):
        user_like = self.db.find_one({"_id":id, "likes.user_id": user_id})
        if not user_like:
            self.db.update_one({"_id":id }, {"$push": {"likes": {"user_id": user_id}}})
            return self.find_by_id(id)
        self.db.update_one({"_id":id, "likes.user_id": user_id }, {"$pull": {"likes": {"user_id": user_id}}})
        return self.find_by_id(id)