from pyramid.exceptions import Forbidden
from pyramid.security import authenticated_userid, effective_principals
from pyramid.view import view_config

from cornice import Service
from cornice.resource import resource, view

from cornice.validators import colander_body_validator

import json
from bson import json_util, ObjectId

from anchor_loans.services.gallery_images_service import GalleryImagesService
from anchor_loans.services.user_service import UserService

from anchor_loans.validators.schemas import SignupSchema
from anchor_loans.validators import user_exists, user_not_exists, \
                                    is_authenticated, is_admin

cors_policy = dict(enabled=True,
            #   headers=('X-My-Header', 'Content-Type'),
              origins=('*'),
            #   credentials=True,
              max_age=42)

@resource(collection_path="gallery", path="gallery/{id}", cors_policy=cors_policy)
class ImageGalleryView():
    def __init__(self, request, context=None):
        self.request = request

    def __acl__(self):
        return [(Allow, Everyone, 'everything')]

    def collection_get(self):
        gi_service = GalleryImagesService(self.request.db)

        if self.request.authenticated_userid:
            user_service = UserService(self.request.db)
            user = user_service.find_by_id(ObjectId(self.request.authenticated_userid))
            if(user['role'] == 'admin'):
                gallery_images = gi_service.list_images()
            else:
                gallery_images = gi_service.list_approved_images()
        else:
            gallery_images = gi_service.list_approved_images()
        return {
            'results': json.loads(json_util.dumps(list(gallery_images)))
        }

    @view(validators=(is_authenticated,))
    def collection_post(self):
        file = self.request.POST['file']
        user_id = self.request.authenticated_userid
        image_inserted = GalleryImagesService(self.request.db).save(file, user_id)
        
        return json.loads(json.dumps(image_inserted, default=json_util.default))

    def delete(self):
        return GalleryImagesService(self.request.db)\
            .delete(ObjectId(self.request.matchdict['id']))


register_service = Service(name='register', path='/register', 
    description="Register an user", cors_policy=cors_policy)
login_service = Service(name='login', path='/login', description="Login", cors_policy=cors_policy)
add_like_service = Service(name='add_like', path='/gallery/{id}/like', 
    description="Add like to an image", cors_policy=cors_policy)
approve_service = Service(name='approve', path='/gallery/{id}/approve', 
    description="Aprove an image", cors_policy=cors_policy)

@add_like_service.put(accept="application/json",
    validators=(is_authenticated))
def add_like_view(request):
    service = GalleryImagesService(request.db)
    image_id = ObjectId(request.matchdict['id'])
    user_id = request.authenticated_userid
    liked_image = service.add_like(image_id, user_id)

    return json.loads(json.dumps(liked_image, default=json_util.default))

@approve_service.put(accept="application/json",
    validators=(is_authenticated, is_admin))
def approve_view(request):
    service = GalleryImagesService(request.db)
    image_id = ObjectId(request.matchdict['id'])
    user_id = request.authenticated_userid
    approved_image = service.approve(image_id, user_id)

    return json.loads(json.dumps(approved_image, default=json_util.default))


@register_service.post(
    accept="application/json",
    schema=SignupSchema(), 
    validators=(colander_body_validator, user_exists))
def register_view(request):
    user_data = json.loads(request.body)
    user = UserService(request.db).save(user_data)
    user_id = json.loads(json.dumps(user['_id'], default=json_util.default))['$oid']
    
    if user:
        return {
            "id": user_id,
            "username": user['username'],
            "role": user['role'],
            "token":request.create_jwt_token(user_id, 
                role=user['role'])
        }

@login_service.post(
    accept="application/json",
    schema=SignupSchema(), 
    validators=(colander_body_validator, user_not_exists))
def login_view(request):
    body = json.loads(request.body)
    username = body['username']
    password = body['password']
    user = UserService(request.db).find_by_username_and_password(username, password)
    user_id = json.loads(json.dumps(user['_id'], default=json_util.default))['$oid']
    
    if user:
        return {
            "id": user_id,
            "username": user['username'],
            "role": user['role'],
            "token":request.create_jwt_token(user_id, 
                role=user['role'])
        }

    request.errors.add('body', 'password', 'Password is incorrect')

