import colander

class SignupSchema(colander.MappingSchema):
    username = colander.SchemaNode(colander.String())
    password = colander.SchemaNode(colander.String(), validator=colander.Regex("\w{6,}"))
