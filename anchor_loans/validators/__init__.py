import json
from bson import ObjectId
from anchor_loans.services.user_service import UserService

def is_authenticated(request,**kargs):
    if not request.authenticated_userid:
        request.errors.add('header', 'token', 'Not authenticated')
        request.errors.status = 401
    else:
        service = UserService(request.db)
        request.user = service.find_by_id(ObjectId(request.authenticated_userid))

def is_admin(request,**kargs):
    if getattr(request, 'user', False) and not request.user['role'] == 'admin':
        request.errors.add('header', 'token', 'Not authorized')
        request.errors.status = 401

def user_exists(request,**kargs):
    service = UserService(request.db)
    body = json.loads(request.body)
    if service.find_by_username(body['username']):
        request.errors.add('body', 'username', 'User already exists!')
        request.errors.status = 400

def user_not_exists(request,**kargs):
    service = UserService(request.db)
    body = json.loads(request.body)
    if body['username'] and not service.find_by_username(body['username']):
        request.errors.add('body', 'username', 'User do not exists!')
        request.errors.status = 400